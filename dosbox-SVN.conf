# Este es el archivo de configuración de DOSBox SVN.
# Las líneas que comienzan con # son comentarios y son ignoradas por DOSBox.
# Son usadas para documentar brevemente el efecto de cada opción.

[sdl]
#       fullscreen: Iniciar DOSBox directamente en pantalla completa. (Presionar ALT-Intro para
#                     regresar)
#       fulldouble: Usar doble búfer en pantalla completa. Puede reducir el parpadeo de la
#                     pantalla, pero también puede ralentizar a DOSBox.
#   fullresolution: Qué resolución usar para pantalla completa: 'original', 'desktop' o un tamaño
#                     fijo (ej: 1024x768).
#                     Usar la resolución nativa de su monitor con aspect=true puede dar los mejores
#                     resultados.
#                     Si ve una ventana chica en una pantalla grande, pruebe una salida distinta a
#                     surface.
#                     En Windows 10 con la escala de pantalla establecida en un valor superior a
#                     100% se recomienda usar un valor más bajo de full o windowresolution, para
#                     evitar problemas de tamaño de ventana.
# windowresolution: Escalar la ventana a esta resolución si el dispositivo de salida soporta
#                     escalado por hardware.
#                     (¡output=surface no lo soporta!)
#           output: Qué sistema de video usar para la salida.
#                     Valores posibles: surface, overlay, opengl, openglnb.
#         autolock: Capturar automáticamente el cursor al hacer clic en la pantalla.
#                     (Presione CTRL-F10 para liberarlo)
#      sensitivity: Sensibilidad del ratón. El segundo parámetro opcional especifica la
#                     sensibilidad vertical (ej: 100,-50).
#      waitonerror: Esperar antes de cerrar la consola si DOSBox tiene un error.
#         priority: Niveles de prioridad para DOSBox.
#                     La segunda entrada detrás de la coma es para cuando DOSBox no tiene foco o
#                     está minimizado.
#                     'pause' sólo es válido para la segunda entrada.
#                     Valores posibles: lowest, lower, normal, higher, highest, pause.
#       mapperfile: Archivo usado para cargar y guardar las asignaciones de tecla o evento.
#                     Resetmapper sólo funciona con el valor predeterminado.
#     usescancodes: Evitar el uso de "symkeys", puede no funcionar en todos los sistemas
#                     operativos.

fullscreen       = false
fulldouble       = false
fullresolution   = original
windowresolution = original
output           = surface
autolock         = true
sensitivity      = 100
waitonerror      = true
priority         = higher,normal
mapperfile       = mapper-SVN.map
usescancodes     = true

[dosbox]
# language: Seleccionar otro archivo de idioma.
#  machine: Tipo de máquina que DOSBox intenta emular.
#             Valores posibles: hercules, cga, tandy, pcjr, ega, vgaonly, svga_s3, svga_et3000, svga_et4000, svga_paradise, vesa_nolfb, vesa_oldvbe.
# captures: Directorio donde se capturan cosas como la pantalla, MIDI y wave.
#  memsize: Cantidad de memoria en megabytes que tiene DOSBox.
#             Es preferible mantener el valor predeterminado para evitar problemas con
#             algunos juegos, aunque algunos pocos pueden requerir un valor más alto.
#             Por lo general, no hay mejora de velocidad al aumentar este valor.

language = spanish.lng
machine  = svga_s3
captures = capture
memsize  = 16

[render]
# frameskip: Cuántos fotogramas DOSBox saltea antes de dibujar uno.
#    aspect: Realizar corrección de aspecto.
#              ¡Si el método de salida no soporta escalado esto puede ralentizar las cosas!
#    scaler: Escalador usado para ampliar o mejorar los modos de baja resolución.
#              Si se agrega 'forced', el escalador será usado incluso si el resultado pudiera
#              no ser el deseado.
#              En el modo en pantalla completa, algunos escaladores pueden mostrar bordes
#              negros para ajustarse a la resolución. Para llenar la pantalla completamente,
#              dependiendo de su hardware, puede probar con diferentes valores de 'scaler' o
#              'fullresolution'.
#              Valores posibles: none, normal2x, normal3x, advmame2x, advmame3x, advinterp2x, advinterp3x, hq2x, hq3x, 2xsai, super2xsai, supereagle, tv2x, tv3x, rgb2x, rgb3x, scan2x, scan3x.
#  glshader: Ruta a la fuente del sombreador ("shader") GLSL a usar con la salida OpenGL
#              ('none' para deshabilitarla).
#              Puede ser una ruta absoluta, un archivo en el subdirectorio "glshaders" del
#              directorio de configuración de DOSBox, o uno de los shaders incorporados:
#              advinterp2x, advinterp3x, advmame2x, advmame3x, rgb2x, rgb3x, scan2x, scan3x,
#              tv2x, tv3x, sharp.

frameskip = 0
aspect    = false
scaler    = normal2x
glshader  = none

[cpu]
#      core: Núcleo de procesador usado en la emulación. 'auto' cambiará a dynamic si está
#              disponible y si es
#              apropiado.
#              Valores posibles: auto, dynamic, normal, simple.
#   cputype: Tipo de procesador usado en la emulación. 'auto' es la opción más rápida.
#              Valores posibles: auto, 386, 386_slow, 486_slow, pentium_slow, 386_prefetch.
#    cycles: Número de instrucciones que DOSBox intenta emular cada milisegundo.
#              Establecer este valor muy alto ocasiona retrasos en el sonido.
#              Los ciclos se pueden establecer de 3 maneras:
#                'auto'          Intenta adivinar lo que necesita un juego.
#                                Generalmente funciona, pero puede fallar en algunos juegos.
#                'fixed #number' Estable un número fijo de ciclos. Generalmente esto es lo
#                                que necesita si 'auto' falla (Ejemplo: fixed 4000).
#                'max'           Asigna tantos ciclos como pueda manejar su computadora.
#              
#              Valores posibles: auto, fixed, max.
#   cycleup: Cantidad de ciclos a disminuir o incrementar con las combinaciones de teclas
#              (CTRL-F11/CTRL-F12).
# cycledown: Si es menor a 100 se toma como porcentaje.

core      = auto
cputype   = auto
cycles    = auto
cycleup   = 10
cycledown = 20

[mixer]
#   nosound: Habilita el modo silencioso, aunque el sonido sigue siendo emulando.
#      rate: Frecuencia de muestreo del mezclador. Establecer más alto que ésta la
#              frecuencia de cualquier dispositivo probablemente reduzca su calidad de
#              sonido.
#              Valores posibles: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
# blocksize: Tamaño de bloque del mezclador. Bloques más grandes pueden ayudar a reducir
#              el tartamudeo del sonido, pero también el sonido estará más retrasado.
#              Valores posibles: 1024, 2048, 4096, 8192, 512, 256.
# prebuffer: Cuántos milisegundos de datos mantener al principio del bloque.

nosound   = false
rate      = 44100
blocksize = 1024
prebuffer = 25

[midi]
#     mpu401: Tipo de MPU-401 a emular.
#               Valores posibles: intelligent, uart, none.
# mididevice: Dispositivo que recibirá los datos MIDI de la interfaz emulada MPU-401.
#               Valores posibles: default, win32, alsa, oss, coreaudio, coremidi, none.
# midiconfig: Opciones especiales de configuración para el controlador del dispositivo.
#               Generalmente es el ID o parte del nombre del dispositivo que quiere usar
#               (puede obtenerlo con mixer /listmidi).
#               O en el caso de coreaudio, puede especificar una fuente ("soundfont").
#               Cuando se usa una Roland MT-32 rev. 0 como dispositivo de salida MIDI, algunos
#               juegos pueden requerir un retraso para prevenir inconvenientes de
#               desbordamiento de búfer. En ese caso, agregue 'delaysysex', por ejemplo:
#               midiconfig=2 delaysysex
#               Lea el manual para más detalles.

mpu401     = intelligent
mididevice = default
midiconfig = 

[sblaster]
#  sbtype: Tipo de Sound Blaster a emular. 'gb' es Game Blaster.
#            Valores posibles: sb1, sb2, sbpro1, sbpro2, sb16, gb, none.
#  sbbase: Dirección de E/S de la tarjeta Sound Blaster.
#            Valores posibles: 220, 240, 260, 280, 2a0, 2c0, 2e0, 300.
#     irq: Número de interrupción (IRQ) de la tarjeta Sound Blaster.
#            Valores posibles: 7, 5, 3, 9, 10, 11, 12.
#     dma: Número de canal DMA de la tarjeta Sound Blaster.
#            Valores posibles: 1, 5, 0, 3, 6, 7.
#    hdma: Número de canal DMA Alto de la tarjeta Sound Blaster.
#            Valores posibles: 1, 5, 0, 3, 6, 7.
# sbmixer: Permitir al mezclador de Sound Blaster modificar al mezclador de DOSBox.
# oplmode: Tipo de emulación OPL.
#            En 'auto' el modo es determinado según el tipo de Sound Blaster.
#            Todos los modos OPL son compatibles con Adlib, excepto 'cms'.
#            Valores posibles: auto, cms, opl2, dualopl2, opl3, opl3gold, none.
#  oplemu: Proveedor de la emulación OPL. 'compat' puede brindar mejor calidad (vea
#            también 'oplrate').
#            Valores posibles: default, compat, fast, mame.
# oplrate: Frecuencia de muestreo de la emulación de música OPL. Use 49716 para la
#            calidad más alta (establezca la frecuencia del mezclador acorde).
#            Valores posibles: 44100, 49716, 48000, 32000, 22050, 16000, 11025, 8000.

sbtype  = sb16
sbbase  = 220
irq     = 7
dma     = 1
hdma    = 5
sbmixer = true
oplmode = auto
oplemu  = default
oplrate = 44100

[gus]
#      gus: Habilitar la emulación de Gravis Ultrasound.
#  gusbase: Dirección base de E/S de la tarjeta Gravis Ultrasound.
#             Valores posibles: 240, 220, 260, 280, 2a0, 2c0, 2e0, 300.
#   gusirq: Número de interrupción (IRQ) de la tarjeta Gravis Ultrasound.
#             Valores posibles: 5, 3, 7, 9, 10, 11, 12.
#   gusdma: Número de canal DMA de la tarjeta Gravis Ultrasound.
#             Valores posibles: 3, 0, 1, 5, 6, 7.
# ultradir: Ruta del directorio Ultrasound.
#             En este directorio debería haber un subdirectorio MIDI que contenga los
#             archivos de parches para la reproducción GUS.
#             Los parches usados con Timidity deberían funcionar bien.

gus      = false
gusbase  = 240
gusirq   = 5
gusdma   = 3
ultradir = C:\ULTRASND

[speaker]
# pcspeaker: Habilitar la emulación de altavoz interno.
#    pcrate: Frecuencia de muestreo de la generación de sonido del altavoz interno.
#              Valores posibles: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#     tandy: Habilitar la emulación del sistema de sonido Tandy.
#              Para 'auto', la emulación sólo está presente cuando machine está establecido
#              en 'tandy'.
#              Valores posibles: auto, on, off.
# tandyrate: Frecuencia de muestreo de la generación de Tandy 3-Voice.
#              Valores posibles: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#    disney: Habilitar la emulación de Disney Sound Source (compatible con Covox Voice
#              Master y Speech Thing).

pcspeaker = true
pcrate    = 44100
tandy     = auto
tandyrate = 44100
disney    = true

[joystick]
#  joysticktype: Tipo de joystick a emular:
#                  auto (predeterminado, elige la emulación dependiendo de los joysticks reales),
#                  none (deshabilita la emulación de joystick),
#                  2axis (soporta dos joysticks),
#                  4axis (soporta un joystick, el primero detectado es usado),
#                  4axis_2 (soporta un joystick, el segundo detectado es usado),
#                  fcs (Thrustmaster),
#                  ch (CH Flightstick).
#                  (Recuerde restablecer el archivo de asignaciones de DOSBox si lo guardó antes)
#                  Valores posibles: auto, 2axis, 4axis, 4axis_2, fcs, ch, none.
#         timed: Habilitar intervalos cronometrados para los ejes. Experimente con esta opción
#                  si su joystick se desvía.
#      autofire: Disparar continuamente mientras se mantenga el botón presionado.
#        swap34: Intercambiar el 3er y el 4to eje. Puede ser útil para ciertos joysticks.
#    buttonwrap: Habilitar el ajuste de los botones al número de botones emulados.
# circularinput: Habilitar la traducción de entrada circular a salida cuadrada.
#                  Intente habilitar esta opción si su palanca analógica izquierda sólo puede
#                  moverse en círculo.
#      deadzone: Qué porcentaje de movimiento ignorar. 100 trata a la palanca como digital.

joysticktype  = auto
timed         = true
autofire      = false
swap34        = false
buttonwrap    = false
circularinput = false
deadzone      = 10

[serial]
# serial1: Establecer el tipo de dispositivo conectado al puerto COM.
#            Puede ser disabled, dummy, modem, nullmodem, directserial.
#            Los parámetros adicionales deben estar en la misma línea en la forma
#            parámetro:valor. 'parámetro' es opcional, y para todos los tipos es el número
#            de interrupción (IRQ).
#            para directserial: realport (requerido), rxdelay (opcional).
#                              (realport:COM1 realport:ttyS0).
#            para modem: listenport (opcional).
#            para nullmodem: server, rxdelay, txdelay, telnet, usedtr,
#                            transparent, port, inhsocket (todos opcionales).
#            Ejemplo: serial1=modem listenport:5000
#            Valores posibles: dummy, disabled, modem, nullmodem, directserial.
# serial2: Vea 'serial1'
#            Valores posibles: dummy, disabled, modem, nullmodem, directserial.
# serial3: Vea 'serial1'
#            Valores posibles: dummy, disabled, modem, nullmodem, directserial.
# serial4: Vea 'serial1'
#            Valores posibles: dummy, disabled, modem, nullmodem, directserial.

serial1 = dummy
serial2 = dummy
serial3 = disabled
serial4 = disabled

[dos]
#            xms: Habilitar soporte para XMS.
#            ems: Habilitar soporte para EMS.
#                   Como predeterminado (=true) provee la mejor compatibilidad, pero ciertas
#                   aplicaciones pueden correr mejor con otras opciones, o requerir que el soporte
#                   para EMS esté deshabilitado (=false) para funcionar.
#                   Valores posibles: true, emsboard, emm386, false.
#            umb: Habilitar soporte para UMB.
# keyboardlayout: Código de idioma de la distribución del teclado (o 'none').

xms            = true
ems            = true
umb            = true
keyboardlayout = auto

[ipx]
# ipx: Habilitar emulación de IPX sobre UDP/IP.

ipx = false

[autoexec]
# Las líneas en esta sección serán ejecutadas al inicio.
# Puede poner sus líneas MOUNT acá.


